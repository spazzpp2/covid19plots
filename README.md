### Popular plots

[**Bundesländer:**](https://codeberg.org/spazzpp2/covid19plots/src/branch/main/doc/bundeslander.md) An overview of the German districts.

### Details

#### Sources

[**Sources:**](https://codeberg.org/spazzpp2/covid19plots/src/branch/main/doc/sources.md) An overview of data sources that are supported by [scrape](https://codeberg.org/spazzpp2/covid19plots/src/branch/main/bin/scrape/).

#### Correlations

[**Scatter-Matrix:**](https://codeberg.org/spazzpp2/covid19plots/src/branch/main/scatter_matrix.ipynb) Does the number of cases in intensive care correlate well with the number positive diagnoses? Also, how many days remain until death?

~[**Correlate World Data:**](https://codeberg.org/spazzpp2/covid19plots/src/branch/main/doc/correlate.md) Where in the world hits COVID-19 the hardest and why?~

#### Trends

[**Reproduction:**](https://codeberg.org/spazzpp2/covid19plots/src/branch/main/doc/repro.md) How fast is the virus spreading?

[**Lethality:**](https://codeberg.org/spazzpp2/covid19plots/src/branch/main/doc/lethality.md) How likely does one die after a positive COVID-19 diagnosis?

[**Countries in Comparison:**](https://codeberg.org/spazzpp2/covid19plots/src/branch/main/doc/countries_in_comparison.md) Did you know that apparently Sweden lost 2.3 times more people per million during the COVID-19 pandemics than Germany?

[**Total VS Week:**](https://codeberg.org/spazzpp2/covid19plots/src/branch/main/doc/sum_vs_window.md) It's a simple thought: The more people have been infected, the more people should get infected the next week. However, as soon the relation ceases to hold, the vicious circle stops. I plot [those graphs](https://aatishb.com/covidtrends/) for German federal states.

~**German Registry for Intensive Care (DIVI):** Did German hospitals exceed their [capacity](https://www.intensivregister.de/#/intensivregister) recently? I scraped their data for plots.~

~[**German Headlines**](https://codeberg.org/spazzpp2/covid19plots/src/branch/main/headlines.ipynb) How did media react to COVID-19?~

#### Speculation

[**Future Projection:**](https://codeberg.org/spazzpp2/covid19plots/src/branch/main/doc/projection.md) What will the COVID-19 future look like? The same *non-exponential* model is used in [[1]](https://www.sciencedirect.com/science/article/pii/S1201971220303039) and [[2]](https://medium.com/analytics-vidhya/predicting-the-spread-of-covid-19-coronavirus-in-us-daily-updates-4de238ad8c26)

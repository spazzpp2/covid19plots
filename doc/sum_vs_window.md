# Total VS Week

It has been claimed that in [a curve like these](https://aatishb.com/covidtrends/) signals a stopping pandemy: The more people get infected, the more people should get infected per week. However, if that trend ceases to continue, the vicious circle has been stopped.

## Germany (total)

![total](../img/sum-vs-window-total.svg)

## German federal states

![federal](../img/sum-vs-window-feds.svg)

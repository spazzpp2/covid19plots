### Projection attempts

#### Naiive Logistic continuation

Assume that the whole population is available for infection. Use the most recent [logistic r-values](https://codeberg.org/spazzpp2/covid19plots/src/branch/master/doc/repro.md) to project into the future.
To comply with the wave restarts, the infected population is being reset every 1st of July.

##### Germany
![Projection char](../img/projection.svg)

##### German federal states
![Projection char](../img/projection-bl.svg)

##### United States of America
![Projection char](../img/projection-us.svg)

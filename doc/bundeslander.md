### Bundesländer

#### Reproduction
[Explanation](https://codeberg.org/spazzpp2/covid19plots/src/branch/main/doc/repro.md).
![German districts Logistic rate and weekly incidence)](../img/weekly_and_logistic.svg)

![German districts ranking over time](../img/plot_rank.svg)

#### Lethality
[Explanation](https://codeberg.org/spazzpp2/covid19plots/src/branch/main/doc/lethality.md).
![Lethality chart for Germany](../img/lethality.svg)

#### Projection
[Explanation](https://codeberg.org/spazzpp2/covid19plots/src/branch/main/doc/projection.md).
![Projection char](../img/projection-bl.svg)

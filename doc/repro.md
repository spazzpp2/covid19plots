### Reproduction rate

Here, the reproduction rate is computed using the [logistic map](https://en.wikipedia.org/wiki/Logistic_map):

`n(t+1) = r*n(t)*(1-n(t))`

`r = n(t+1)/n(t)/(1-n(t))`

As `r` is the propability of how likely the rest of the population is being infected by those already infected, I think it can be seen as a value of how well the considered society is prepared. At least for delta's predecessors, the official weekly reproduction rate (incidence) lagged the logistic r-value. 

![Weekly & Logistic Rates Chart](../img/weekly_and_logistic.svg)

#### German federated states race against the pandemic

![German districts ranking over time](../img/plot_rank.svg)

##### Current positions
![German districts in the race to 0.0](../img/rki_bars.svg)
![German districts in the race to 1.0](../img/logistic_bars.svg)
![German districts in the race to 0.0](../img/weekly_bars.svg)

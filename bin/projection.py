#!/usr/bin/env python

import pandas as pd
import matplotlib.pyplot as plt
import datetime

from common import *
from scrape import entorb
from repro import polynomial_r

FUTURE_RANGE = 1 * 365
NOW = datetime.datetime.now()
LAST_SUMMER = '' + str(NOW.year if NOW.month>7 else NOW.year-1) + '-02-20' # seasonal model
LAST_SUMMER = '2022-09-16' # Variant after BA4


def plot_projections(names, des, populations, col='Cases', future_range=FUTURE_RANGE, LOG=False):
    """
    Plot graphs of four rates.
    """
    fig, axes = plt.subplots(nrows=int(len(des)/4), ncols=4, sharex=True, sharey=True, figsize=(16,16))
    for name, de, population, ax in zip(names, des, populations, axes.flat):
        de = de.drop(columns=[c for c in de.columns if c != col and c != col+"_New"]).rolling('7D').mean()

        now = de.reset_index()['Date'].tail(1).values[0]

        date_range = pd.date_range(start=now, \
                periods=FUTURE_RANGE/1, \
                freq="%dD" % 1)

        done = set()

        for interval in [14]:
            df = pd.DataFrame(de)
            lastlow = df[df.index >= LAST_SUMMER].first('1D').mean()/2 # LAST_SUMMER HALVED
            df -= lastlow
            rs = polynomial_r(df, population=population, generation_time=1)
            
            r = rs[col].last("%dD" % interval).max()

            n = list()
            for i, _ in enumerate(date_range):
                if i == 0:
                    n.append(df[col].last("%dD" % interval).max())
                    continue
                n.append(r * n[-1] * (1. - n[-1]/population))

            proj_col = "Projected %s (last %dD max rate)" % (col, interval)
            proj = pd.DataFrame({ 'Date': date_range, proj_col: n }) \
                    .set_index('Date')
            proj[proj_col] += lastlow['Cases']
            proj[proj_col+"_New"] = proj[proj_col].diff(1)
            df = de

            for d,c in [((df*100000/population).rolling('7D').sum(), col), (proj*700000/population, proj_col)]:
                if c in done:
                    continue
                #d[c].plot(ax=ax, logy=LOG, label=c)
                d[c+"_New"].plot(ax=ax, logy=LOG, label=c, title=name)
                done.add(c)

            ax.grid()
            count = names.index(name)
            if count==4:
                ax.set_ylabel("Weekly cases per 100.000 people")
            if count==0:
                ax.legend(loc='upper left')

    fig.suptitle(("COVID-19 spread projection using Logistic map\n"
        "assuming a carrying capacity of the complete population (reset on %s) and a tick time of %dD.\n"
        "created: %s"
        " author: @spazzpp2,"
        " source: RKI & JHU CSSE via @entorb.") \
                % (LAST_SUMMER, DAYS_INFECTION_TILL_SYMPTOM, str(now)[:10]))

    fig.tight_layout()#rect=(0, 0.03, 1, 0.95))
    fig.autofmt_xdate()
    return fig


def plot_projection(de, population, col='Cases', future_range=FUTURE_RANGE, LOG=False):
    """
    Plot graphs of four rates.
    """
    de = de.drop(columns=[c for c in de.columns if c != col and c != col+"_New"]).rolling('7D').mean()

    now = de.reset_index()['Date'].tail(1).values[0]

    date_range = pd.date_range(start=now, \
            periods=FUTURE_RANGE/1, \
            freq="%dD" % 1)

    done = set()

    fig, axes = plt.subplots(nrows=2, sharex=True, figsize=(10,9))
    for interval in [14]:
        df = pd.DataFrame(de)
        lastlow = df[df.index <= LAST_SUMMER].last('1D').mean()
        df -= lastlow
        rs = polynomial_r(df, population=population, generation_time=1)
        
        r = rs[col].last("%dD" % interval).max()

        n = list()
        for i, _ in enumerate(date_range):
            if i == 0:
                n.append(df[col].tail(1).values[0])
                continue
            n.append(r * n[-1] * (1. - n[-1]/population))

        proj_col = "Projected %s (last %dD avg rate)" % (col, interval)
        proj = pd.DataFrame({ 'Date': date_range, proj_col: n }) \
                .set_index('Date')
        proj[proj_col] += lastlow['Cases']
        proj[proj_col+"_New"] = proj[proj_col].diff(1)
        df = de

        for d,c in [(df, col), (proj, proj_col)]:
            if c in done:
                continue
            d[c].plot(ax=axes[0], logy=LOG, label=c)
            d[c+"_New"].plot(ax=axes[1], logy=LOG, label=c)
            done.add(c)

    axes[0].grid()
    axes[0].set_ylabel("Total")
    axes[1].grid()
    axes[1].set_ylabel("Daily")
    axes[1].legend(handles=axes[1].lines, loc='best')

    fig.suptitle(("COVID-19 spread projection using Logistic map\n"
        "assuming a carrying capacity of K=%dM people and dt=%dD tick time.\n"
        "created: %s"
        " author: @spazzpp2,"
        " source: RKI & JHU CSSE via @entorb.") \
                % (population/1e6, DAYS_INFECTION_TILL_SYMPTOM, str(now)[:10]))

    fig.tight_layout()
    fig.autofmt_xdate()
    return fig


def plot_future_unld(de, population, col='Cases', future_range=FUTURE_RANGE, LOG=False):
    """
    """
    fig, axes = plt.subplots(nrows=2, sharex=True, figsize=(10,9))

    FACTOR = 3
    
    df = de * FACTOR
    rs = polynomial_r(df, population=population, generation_time=1)
    
    contemp_r = rs[col].values[-1]
    r = contemp_r

    now = de.reset_index()['Date'].tail(1).values[0]
    date_range = pd.date_range(start=now, \
            periods=FUTURE_RANGE/1, \
            freq="%dD" % 1)

    n = list()
    for i, _ in enumerate(date_range):
        if i == 0:
            n.append(df[col].tail(1).values[0])
            continue
        # apply gov rules at turning point when weekly infections per 100000 sink below 100
        if i > 8 and 50 <= 1e5*(n[-7]-n[-1])/population <= 100:
            r = 1.6
        else:
            r = contemp_r
        n.append(max(n[-1], r * n[-1] * (1. - n[-1]/population)))

    proj_col = "Projected %s x%d (last 1D avg rate)" % (col, FACTOR)
    proj = pd.DataFrame({ 'Date': date_range, proj_col: n }) \
            .set_index('Date')
    proj[proj_col+"_New"] = proj[proj_col].diff(1)

    for d, c in [(df, col), (proj, proj_col)]:
        d[c].plot(ax=axes[0], logy=LOG, label="%s x%d" % (c, FACTOR))
        d[c+"_New"].plot(ax=axes[1], logy=LOG, label="%s x%d" % (c, FACTOR))

    axes[0].grid()
    axes[0].set_ylabel("Total")
    axes[1].grid()
    axes[1].set_ylabel("Daily")
    axes[1].legend(handles=axes[1].lines, loc='best')

    fig.suptitle(("COVID-19 spread projection using Logistic map\n"
        "assuming a carrying capacity of K=%dM people and dt=%dD tick time.\n"
        "created: %s"
        " author: @spazzpp2,"
        " source: JHU CSSE via @entorb & @pomber.") \
                % (population/1e6, DAYS_INFECTION_TILL_SYMPTOM, str(now)[:10]))

    fig.tight_layout()
    fig.autofmt_xdate()
    return fig


def main():
    import sys
    if len(sys.argv) < 3:
        print("USAGE: %s OUTFILEprojection1 OUTFILEfuture_unld2" % sys.argv[0])
        sys.exit(2)
    de = entorb.to_dataframe('DE-total')
    with plt.style.context('ggplot'):
        plot_projection(de, DE_POPULATION, LOG=False).savefig(sys.argv[1], bbox_inches='tight')
        plot_projections([n for _, n in DE_STATE_NAMES.items()], \
                [entorb.to_dataframe(s) for s in DE_STATE_NAMES], \
                [DE_STATE_POPULATION[DE_STATE_NAMES[s]] for s in DE_STATE_NAMES], LOG=False) \
                .savefig(sys.argv[2], bbox_inches='tight')
        plot_projection(de=entorb.to_dataframe(nation='US'), population=329e6, LOG=False).savefig(sys.argv[3], bbox_inches='tight')


if __name__ == "__main__":
    main()


#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from matplotlib import dates as mdates

from scrape import entorb
from common import DE_POPULATION, DE_STATE_POPULATION, DE_STATE_NAMES, \
        DAYS_INFECTION_TILL_SYMPTOM, DAYS_SYMPTOMS_TILL_DEATH


def polynomial_r(df, population=DE_POPULATION,
        generation_time=DAYS_INFECTION_TILL_SYMPTOM):
    """
    Reproduction rate using logistic map

    `n(t+1) = r * n(t) * (1 - n(t)/K)`

    `r = n(t) / n(t-1) / (1 - n(t-1)/K)`

    Args:
        df (DataFrame): n(t+1)
        population (int): total infectable population K
        generation_time: delay between infections in days

    Return:
        DataFrame with reproduction rates for `Deaths` and
        `Cases`
    """
    rs = pd.DataFrame()
    for col in ['Cases']:
        try:
            earlier = df[col].shift(generation_time)
            #rs[col] = (df[col] * population) / (earlier * (-df[col] + population))
            rs[col] = df[col] / earlier / (1. - earlier / population)
        except KeyError as e:
            pass
    return rs


def rki_r(df, generation_time=DAYS_INFECTION_TILL_SYMPTOM):
    """
    Reproduction rate using RKI method. That is: `r =
    C(one generation time earlier) / C(now)` For each `C`
    sum up Cases over the last generation time.

    Args:
        df (DataFrame): data
        generation_time: delay between infections in days

    Return:
        DataFrame with reproduction rates for `Deaths` and
        `Cases`
    """
    rs = pd.DataFrame()
    for col in ['Deaths', 'Cases']:
        try:
            rolled = df[col+'_New'].rolling("%dD" % generation_time).sum()
            earlier = rolled.shift(generation_time)
            rs[col] = rolled / earlier
        except KeyError as e:
            pass
    return rs


def weekly_r(df, population):
    """
    Number of infections per 100k people in a week. Resamples to 7 days.

    Args:
        df (DataFrame): data
        generation_time: delay between infections in days

    Return:
        DataFrame with reproduction rates for `Deaths` and `Cases`
    """
    rs = pd.DataFrame()
    for col in ['Deaths', 'Cases']:
        rs[col] = df[col].diff().rolling("7D").sum() / population * 1e5
    return rs


def plot_rki_and_logistic_total(state='DE-total'):
    """
    Plot rki and logistic rates for Germany.

    Args:
        state: ISO code of German district, DE-total or Country

    Return:
        Figure
    """
    if __name__ == "__main__":
        global cached_dfs
    else:
        cached_dfs = {}
    de = None
    if state not in cached_dfs:
        de = entorb.to_dataframe(state)
        cached_dfs[state] = de
    else:
        de = cached_dfs[state]

    fig, ax = plt.subplots()
    ax2 = ax.twinx()

    l = ax2.plot(de.index, de['Cases_New'].rolling('7D').mean(), color='k',
            label='New Cases rolling week')
    ax2.plot(de.index, de['Cases_New'], linestyle=':', color=l[0].get_color(),
            label='New Cases')

    poly = polynomial_r(de)['Cases']
    l = ax.plot(de.index, poly.rolling('7D').mean(),
            label='Logistic rate rolling week')
    ax.plot(de.index, poly, linestyle=':', color=l[0].get_color(),
            label='Logistic rate')

    rki = rki_r(de)['Cases']
    l = ax.plot(de.index, rki.rolling('7D').mean(),
            label='RKI rate rolling week')
    ax.plot(de.index, rki, linestyle=':', color=l[0].get_color(),
            label='RKI rate')

    fig.suptitle(("COVID-19 coefficient r of the logistic map and weekly incidence\n"
        "assuming a carrying capacity of the complete population and a tick time of dt=%dD.\n"
        "created: %s"
        " author: @spazzpp2,"
        " source: RKI & JHU CSSE via @entorb.") \
                % (DAYS_INFECTION_TILL_SYMPTOM, str(datetime.now().strftime('%Y-%m-%d'))[:10]))
    plt.legend(handles=ax.lines+ax2.lines)
    ax.axhline(1.0, color='g', alpha=0.5)
    fig.set_size_inches(16,9)
    fig.tight_layout()
    return fig


def plot_r(col='Cases', population=DE_POPULATION):
    if __name__ == "__main__":
        global cached_dfs
    else:
        cached_dfs = {}
    lasts = []
    lasts_rki = []
    areas = sorted([x for x in DE_STATE_NAMES])
    fig, axes = plt.subplots(nrows=4, ncols=4, sharex=True, sharey=True)
    for i, (ax, area) in enumerate(zip(axes.flat, areas)):
        de = None
        if area in cached_dfs:
            de = cached_dfs[area]
        else:
            de = entorb.to_dataframe(area).rolling('7D').mean()
            cached_dfs[area] = de

        rs = polynomial_r(de, population[DE_STATE_NAMES[area]])
        rs = rs.rolling('7D').mean()
        lasts.append(rs[col].tail(1).values[0])

        ax = rs[col].plot(ax=ax,
                title="%s (%d Ew.)" % (DE_STATE_NAMES[area],
                    population[DE_STATE_NAMES[area]]))
        
        rs = rki_r(de)
        lasts_rki.append(rs[col].tail(1).values[0])
        rs[col].plot(ax=ax)
        
    fig.suptitle(("COVID-19 coefficient r of the logistic map\n"
        "assuming a carrying capacity of the complete population and a tick time of dt=%dD.\n"
        "created: %s"
        " author: @spazzpp2,"
        " source: RKI & JHU CSSE via @entorb.") \
                % (DAYS_INFECTION_TILL_SYMPTOM, str(datetime.now().strftime('%Y-%m-%d'))[:10]))
    fig.set_size_inches(16,16)
    fig.tight_layout()
    return fig, lasts, lasts_rki


def plot_weekly_r(col='Cases', ncols=4):
    """
    Plot of <col> per week
    
    Args:
        col: What to plot. 'Cases' by default.
        ncols: Columns of charts of resulting figure.

    Return:
        Figure
    """
    if __name__ == "__main__":
        global cached_dfs
    else:
        cached_dfs = {}
    areas = sorted([x for x in DE_STATE_NAMES])
    fig, axes = plt.subplots(nrows=4, ncols=4, sharex=True, sharey=True)
    for i, (ax, area) in enumerate(zip(axes.flat, areas)):
        de = None
        if area in cached_dfs:
            de = cached_dfs[area]
        else:
            de = entorb.to_dataframe(area)
            cached_dfs[area] = de
        rs = weekly_r(de, DE_STATE_POPULATION[DE_STATE_NAMES[area]])
        rs[col].plot(ax=ax, title=DE_STATE_NAMES[area])
    fig.suptitle(("COVID-19 weekly cases per 100.000 people\n"
        "assuming a carrying capacity of the complete population and a tick time of dt=%dD.\n"
        "created: %s"
        " author: @spazzpp2,"
        " source: RKI & JHU CSSE via @entorb.") \
                % (DAYS_INFECTION_TILL_SYMPTOM, str(datetime.now().strftime('%Y-%m-%d'))[:10]))
    fig.set_size_inches(16,16)
    fig.tight_layout()
    return fig


def plot_rki_and_logistic(col='Cases', ncols=4, population=DE_STATE_POPULATION):
    """
    Plot of reproduction of <col>
    
    Args:
        col: What to plot. 'Cases' by default.
        ncols: Columns of charts of resulting figure.

    Return:
        Tuple of a Figure, a list of last logistical rates and a list of last
        rki values
    """
    if __name__ == "__main__":
        global cached_dfs
    else:
        cached_dfs = {}
    areas = sorted([x for x in DE_STATE_NAMES])
    lasts = {'area': areas, 'logistic': [], 'rki': [], 'weekly': []}
    fig, axes = plt.subplots(nrows=4, ncols=4, sharex=True, sharey=True)
    for i, (ax, area) in enumerate(zip(axes.flat, areas)):
        de = None
        if area not in cached_dfs:
            cached_dfs[area] = entorb.to_dataframe(area)
        de = cached_dfs[area].rolling('7D').mean()

        rs = polynomial_r(de, population[DE_STATE_NAMES[area]])
        rs = rs.rolling('7D').mean()
        lasts['logistic'].append(rs[col].values[-1])

        rs[col].plot(ax=ax, label="logistic",
                title="%s (%d Ew.)" % (DE_STATE_NAMES[area],
                    population[DE_STATE_NAMES[area]]), legend=i==0)

        rs = rki_r(de)
        lasts['rki'].append(rs[col].values[-1])
        rs[col].plot(ax=ax, ylim=(1,2), label="rki", sharex=True, sharey=True,
                legend=i==0)

        rs = weekly_r(de, population[DE_STATE_NAMES[area]])
        lasts['weekly'].append(rs[col].values[-1])
        #ax2 = rs[col].plot(ax=ax, label="weekly", sharex=True, sharey=True, logy=True)

    fig.suptitle(("COVID-19 RKI's R_eff and coefficient r of the logistic map\n"
        "assuming a carrying capacity of the complete population and a tick time of dt=%dD.\n"
        "created: %s"
        " author: @spazzpp2,"
        " source: RKI & JHU CSSE via @entorb.") \
                % (DAYS_INFECTION_TILL_SYMPTOM, str(datetime.now().strftime('%Y-%m-%d'))[:10]))
    fig.set_size_inches(16,16)
    return fig, pd.DataFrame(lasts).set_index('area')


def plot_weekly_and_logistic(col='Cases', ncols=4,
        population=DE_STATE_POPULATION):
    """
    Plot of reproduction of <col>
    
    Args:
        col: What to plot. 'Cases' by default.
        ncols: Columns of charts of resulting figure.

    Return:
        Tuple of a Figure, a list of last logistical rates and a list of last
        rki values
    """
    if __name__ == "__main__":
        global cached_dfs
    else:
        cached_dfs = {}
    areas = sorted([x for x in DE_STATE_NAMES])
    lasts = {'area': areas, 'logistic': [], 'rki': [], 'weekly': []}
    maxweekly = 0.
    fig, axes = plt.subplots(nrows=4, ncols=4, sharex=True, sharey=True)
    for i, (ax, area) in enumerate(zip(axes.flat, areas)):
        de = None
        if area not in cached_dfs:
            cached_dfs[area] = entorb.to_dataframe(area)
        de = cached_dfs[area] #.rolling('7D').mean()

        rs = polynomial_r(de, population[DE_STATE_NAMES[area]])
        #rs = rs.rolling('7D').max()
        lasts['logistic'].append(rs[col].values[-1])

        rs[col].plot(ax=ax, label="logistic", ylim=(1., 2.),
                title="%s (%d Ew.)" % (DE_STATE_NAMES[area],
                    population[DE_STATE_NAMES[area]]), legend=i==0,
                logy=True)

        rs = rki_r(de)
        lasts['rki'].append(rs[col].values[-1])
        #rs[col].plot(ax=ax, ylim=(1,2), label="rki", sharex=True, sharey=True)

        rs = weekly_r(de, population[DE_STATE_NAMES[area]])
        maxweekly = max(maxweekly, rs[col].max())
        lasts['weekly'].append(rs[col].values[-1])
        rs[col].plot(ax=ax, label="weekly", secondary_y=True, sharex=True,
                sharey=True, logy=True, legend=i==0)

    for ax in axes.flat:
        ax.right_ax.set_ylim(0, maxweekly)

    fig.suptitle(("COVID-19 coefficient r of the logistic map and weekly incidence\n"
        "assuming a carrying capacity of the complete population and a tick time of dt=%dD.\n"
        "created: %s"
        " author: @spazzpp2,"
        " source: RKI & JHU CSSE via @entorb.") \
                % (DAYS_INFECTION_TILL_SYMPTOM, str(datetime.now().strftime('%Y-%m-%d'))[:10]))
    fig.set_size_inches(16,16)
    return fig, pd.DataFrame(lasts).set_index('area')


def logistic_bars(lasts, title='Infektionen'):
    ax = lasts \
            .drop(columns=[x for x in lasts.columns if x != 'logistic']) \
            .sort_values('logistic') \
            .plot(kind='barh', xlim=(min(1.0, lasts['logistic'].min()),
                    lasts['logistic'].max()), legend=False, grid=False, 
                    title=("Die Bundesländer im Rennen auf R=1.0\nLogistisch,"
                        "%s, Stand: %s") % (title,
                            datetime.now().strftime('%Y-%m-%d')))
    fig = ax.get_figure()
    fig.set_size_inches(9,9)
    return fig


def plot_rank(title='Inzidenz', func=weekly_r,
              states=DE_STATE_NAMES, population=DE_STATE_POPULATION,
              label=None, fig=None, axes=None):
    if __name__ == "__main__":
        global cached_dfs
    else:
        cached_dfs = {}

    if fig is None or axes is None:
        fig, axes = plt.subplots(nrows=4, ncols=4, sharex=True, sharey=True)

    de = None
    for iso, state in states.items():
        if iso not in cached_dfs:
            cached_dfs[iso] = entorb.to_dataframe(iso)
        if de is None:
            de = func(cached_dfs[iso], population=population[state])
            de.drop(columns=[c for c in de.columns if c != 'Cases'])
            de.rename(columns={'Cases': state}, inplace=True)
        else:
            de[state] = func(cached_dfs[iso],
                             population=population[state])['Cases']
    rnk = de.rank(axis=1).rolling('30D').mean()

    ax_idx = 0
    for iso, state in states.items():
        rnk[state].plot(kind='line',
                        ax=axes.flat[ax_idx],
                        title=state, label=label,
                        legend=None)
        ax_idx += 1

    fig.suptitle(title)
    fig.set_size_inches(16, 16)
    fig.tight_layout()
    return fig, axes


def rki_bars(lasts, title='Infektionen'):
    ax = lasts \
            .drop(columns=[x for x in lasts.columns if x != 'rki']) \
            .sort_values('rki') \
            .plot(kind='barh', xlim=(min(1.0, lasts['rki'].min()), 
                    lasts['rki'].max()), legend=False, grid=False, 
                    title=("Die Bundesländer im Rennen auf R=0.0\nFälle nach 4"
                        "Tagen pro Infizierten, %s, Stand: %s") % (title,
                            datetime.now().strftime('%Y-%m-%d')))
    fig = ax.get_figure()
    fig.set_size_inches(9,9)
    return fig


def weekly_bars(lasts, title="Infektionen"):
    ax = lasts \
            .drop(columns=[x for x in lasts.columns if x != 'weekly']) \
            .sort_values('weekly') \
            .plot(kind='barh', xlim=(min(1.0, lasts['weekly'].min()),
                lasts['weekly'].max()),  legend=False, grid=False,
                title=("Die Bundesländer im Rennen auf R=0.0\nFälle pro Woche"
                    "und 100 000 Ew., %s, Stand: %s") % (title,
                        datetime.now().strftime('%Y-%m-%d')))
    fig = ax.get_figure()
    fig.set_size_inches(9,9)
    return fig


def plot_press_chronic():
    if __name__ == "__main__":
        global cached_dfs
    else:
        cached_dfs = {}
    de = None
    if 'DE-total' in cached_dfs:
        de = cached_dfs['DE-total']
    else:
        de = entorb.to_dataframe('DE-total')

    rs1 = polynomial_r(de)
    rs1 = rs1.rolling('7D').mean()
    rs2 = weekly_r(de)
    news = pd.read_csv('data/chronic_de.tsv',
            sep="\\t", usecols=['Datum', 'Ereignis'], engine='python')
    news['Datum'] = pd.to_datetime(news['Datum'], format='%Y-%m-%d')
    news = news.set_index('Datum')

    fig, ax = plt.subplots()
    rs1['Cases'].transpose().plot(ax=ax, label='Logistisch')
    rs2['Cases'].transpose().plot(ax=ax, secondary_x=True,
            label='Wochenfälle je 100 000 Ew.')
    ax.set_title("Fall-Rate. COVID-19-Chronik der FAZ")
    ax.set_yticks(news.index)
    ax.set_ylim(news.index.max() + timedelta(days=4), rs1['Cases'].index.min())
    ax.set_yticklabels(news['Ereignis'])
    ax.grid()
    ax.set_ylabel('')
                 
    plt.legend(loc='lower right')
                 
    fig.set_size_inches(9,16)
    
    return fig


def main():
    import sys
    if len(sys.argv) < 3:
        print("USAGE: %s F OUTFILE [F OUTFILE [...]]" % sys.argv[0])
        print("Values for F:")
        for x in [ \
                plot_rki_and_logistic_total, \
                plot_rki_and_logistic, \
                plot_weekly_and_logistic, \
                logistic_bars, \
                rki_bars, \
                weekly_bars, \
                plot_press_chronic]:
            print("%d: %s%s ", i, x.__name__, x.__doc__)
        sys.exit(2)

    with plt.style.context('ggplot'):
        done = []
        lasts = None
        # create lasts
        for func in [plot_weekly_and_logistic, plot_rki_and_logistic]:
            if func in done:
                continue
            try:
                i = sys.argv.index(func.__name__)
                fig, lasts = func()
                print("saving", sys.argv[i+1])
                fig.savefig(sys.argv[i+1], bbox_inches='tight')
                done.append(func)
            except ValueError:
                pass
        # use lasts
        for func in [logistic_bars, rki_bars, weekly_bars]:
            if func in done:
                continue
            try:
                i = sys.argv.index(func.__name__)
                fig = func(lasts)
                print("saving", sys.argv[i+1])
                fig.savefig(sys.argv[i+1], bbox_inches='tight')
                done.append(func)
            except ValueError:
                pass
        # no args
        for func in [plot_rki_and_logistic_total,
                plot_r, plot_weekly_r]:
            if func in done:
                continue
            try:
                i = sys.argv.index(func.__name__)
                fig = func()
                print("saving", sys.argv[i+1])
                fig.savefig(sys.argv[i+1], bbox_inches='tight')
                done.append(func)
            except ValueError:
                pass
        # ranks
        try:
            i = sys.argv.index("plot_rank")
            fig, axes = plot_rank(title='', label='Logistisch', func=polynomial_r)
            plot_rank(title=('Gleitendes 30-Tage-Mittel des Rankings der'
                             ' Bundesländer über die Zeit\nLogistische Reproduktionsrate und Inzidenz'
                             ' (niederiger ist besser), Quelle: RKI'),
                      func=weekly_r, label='Inzidenz', fig=fig, axes=axes)
            axes.flat[0].legend()
            fig.savefig(sys.argv[i+1]) 
        except ValueError:
            pass
    sys.exit(0)

if __name__ == "__main__":
    cached_dfs = dict()
    main()

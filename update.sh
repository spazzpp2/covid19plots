#!/bin/bash

set -exo pipefail

HERE=$(readlink -f $(dirname $0))
ENVACT="$HERE/../env/bin/activate"

cd "$HERE"

if [ -e $ENVACT ]; then
	source $ENVACT
fi

# plot_press_chronic img/plot_press_chronic.svg
time bin/repro.py \
	plot_rki_and_logistic_total img/rki_and_logistic_total.svg \
	plot_rki_and_logistic img/rki_and_logistic.svg \
    plot_weekly_and_logistic img/weekly_and_logistic.svg \
	logistic_bars img/logistic_bars.svg \
	rki_bars img/rki_bars.svg \
	weekly_bars img/weekly_bars.svg \
	plot_weekly_r img/plot_weekly_r.svg \
	plot_rank img/plot_rank.svg
time bin/lethality.py lethality_mass_plot img/lethality.svg
time bin/projection.py img/projection.svg img/projection-bl.svg img/projection-us.svg
#TODO time bin/divi.py img/divi-hbars.svg "img/divi%(state)s.svg"
# time bin/source_comparison.py img/source_deltas.png
# time bin/correlate.py img/correlate.png
time bin/sum_vs_window.py
time bin/countries_in_comparison.py

git add img/*.*

git commit -m "upd: data"
git push --set-upstream origin main
#git push github

if [ -e $ENVACT ]; then
	deactivate
fi

cd -

